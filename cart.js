const items=[
    {
        "id": 1,
        "name" : "Chocolate",
        "price" : 500, 
    },

    {
        "id": 2,
        "name" : "Black_Forest",
        "price" : 700, 
    },

    {
        "id": 3,
        "name" : "Strawbery",
        "price" : 1000, 
    },

    {
        "id": 4,
        "name" : "Strawbery_Wedding",
        "price" : 500, 
    },

    {
        "id": 5,
        "name" : "Vanilla_Propose",
        "price" : 700, 
    },

    {
        "id": 6,
        "name" : "Wedding_Ice",
        "price" : 1000, 
    },

    {
        "id": 7,
        "name" : "Ice_cake",
        "price" : 500, 
    },

    {
        "id": 8,
        "name" : "chiffon_cake",
        "price" : 700, 
    },

    {
        "id": 9,
        "name" : "Butter_Ice",
        "price" : 1000, 
    },
];

var TotalAmount=0;
var ItemCount=0;

function AddToCart(id) {

    var itemid=id;
    var item=items.find(item => item.id === itemid);
    console.log(item);
    var itemcost=item.price;
    var itemname=item.name;
    var quantity=1;
    ItemCount+=1;
    TotalAmount+=quantity*itemcost;

    var cart=document.getElementById("carttable");
    cart.innerHTML+= 
    `
    <tr>
        <th>${ItemCount}</th>
        <th>${itemname}</th>
        <th>${quantity}</th>
        <th>${itemcost}</th>
        <th>${quantity*itemcost}</th>
    </tr>
    `

    var cartTotal=document.getElementById("cartTotal");
    cartTotal.innerHTML =
    `
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th><b>Total</b></th>
        <th>${TotalAmount}</th>
    </tr>
    `

}

